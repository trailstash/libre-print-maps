# Libre Print Maps

Web maps normally don't print well, as their resolution is much lower than
normal print resolution, not to mention the various other unwanted text and
elements that print along with the map. Print Maps changes that by leveraging
[MapLibre GL JS](https://github.com/maplibre/maplibre-gl-js) to render print
resolution maps in the browser.

## Differences with the original [Print Maps](https://github.com/mpetroff/print-maps)

### MapLibre GL JS
Due to Mapbox's new [anti-FOSS stance](https://github.com/mapbox/mapbox-gl-js/releases/tag/v2.0.0),
this fork of Print Maps is switched to using MapLibre GL JS.

### OSM Carto as only default style

### Custom Styles
There is now an `Custom` option in the style choices, allowing you to enter your own `style.json`
URL.

### PMTiles
For maximum compatibility with custom styles, support for the [`pmtiles://`
protocol](https://protomaps.com/docs/pmtiles) is installed.

### Location
The MapLibre
[GeolocateControl](https://maplibre.org/maplibre-gl-js-docs/api/markers/#geolocatecontrol) is used
instead of always finding the location. And support for location hashes is enabled.

## Options

* Inches or millimeters
* PNG or PDF output (PDF is Letter size for inches, A4 for millimeters)
* Choice of map styles
* Height and width settings
* DPI setting

## Building

Run a local webserver such as `python3 -m http.server`, and open `index.html`.

## Attribution

Attribution of maps is required. See tile provider terms for details.

## License

Print Maps is distributed under the MIT License. For more information, read the
file `COPYING` or peruse the license
[online](https://gitlab.com/trailstash/libre-print-maps/-/blob/main/COPYING).

## Credits

* [Matthew Petroff](http://mpetroff.net/), Original Author
* [Daniel Schep](http://schep.me/), TrailStash changes
* [Mapbox GL JS](https://github.com/mapbox/mapbox-gl-js)
* [FileSaver.js](https://github.com/eligrey/FileSaver.js/)
* [canvas-toBlob.js](https://github.com/eligrey/canvas-toBlob.js)
* [jsPDF](https://github.com/MrRio/jsPDF)
* [Bootstrap](http://getbootstrap.com/)
